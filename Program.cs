﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {

            Dataset p1 = new Dataset("csv.txt");
            Dataset p2 = (Dataset)p1.Clone();

            p1.Print();
            p2.Change();
            p2.Print();
            p1.ClearData();

            p1.Print();
            p2.Print();



        }
    }
}
